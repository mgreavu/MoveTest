#pragma once
#include <cstring>
#include <iostream>

class test_string
{
public:
	static constexpr int DEFAULT_SIZE = 16;

	test_string()
		: m_size(DEFAULT_SIZE)
	{
		std::cout << "test_string() constructor" << std::endl;

		m_buffer = new char[m_size];
		memset(m_buffer, 0, m_size);
	}
	test_string(const char* data)
	{
		std::cout << "test_string(const char* data) constructor" << std::endl;

		m_size = strlen(data) + 1;
		m_buffer = new char[m_size];
		memset(m_buffer, 0, m_size);
		strncpy(m_buffer, data, m_size - 1);
	}

	~test_string()
	{
		delete[] m_buffer;
	}

	// copy constructor
	test_string(const test_string& tc)
	{
		std::cout << "test_string copy constructor" << std::endl;

		m_size = tc.m_size;
		m_buffer = new char[m_size];
		std::copy(tc.m_buffer, tc.m_buffer + tc.m_size, m_buffer);
	}

	// move constructor
	test_string(test_string&& tc) noexcept
	{
		std::cout << "test_string move constructor" << std::endl;

		m_size = tc.m_size;
		m_buffer = tc.m_buffer;
		tc.m_buffer = nullptr;
		tc.m_size = 0;
	}

	// assignment operator
	test_string& operator=(const test_string& other)
	{
		if (this != &other) // protect against invalid self-assignment
		{
			std::cout << "test_string assignment operator" << std::endl;

			// 1: allocate new memory and copy the elements
			char* new_array = new char[other.m_size];
			memset(new_array, 0, other.m_size);
			std::copy(other.m_buffer, other.m_buffer + other.m_size, new_array);

			// 2: deallocate old memory
			delete[] m_buffer;

			// 3: assign the new memory to the object
			m_buffer = new_array;
			m_size = other.m_size;
		}
		// by convention, always return *this
		return *this;
	}

	void print() const
	{
		std::cout << "m_buffer: " << m_buffer << ", size: " << m_size << std::endl;
	}

private:
	char* m_buffer;
	int m_size;
};

class test_string_container
{
public:
	test_string_container(test_string ts)
//		: m_ts(ts)
		: m_ts(std::move(ts))
		, m_id(0)
	{
		std::cout << "test_string_container(test_string ts) constructor" << std::endl;
	}

	test_string_container(const test_string& ts, int id)
		: m_ts(ts)
		, m_id(id)
	{
		std::cout << "test_string_container(const test_string& ts, int id) constructor" << std::endl;
	}

	void print() const
	{
		m_ts.print();
	}

private:
	test_string m_ts;
	int m_id;
};