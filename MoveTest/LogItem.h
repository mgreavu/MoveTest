#pragma once
#include <string>
#include <iostream>

class CLogItem
{
public:
    CLogItem(std::string FirstText, std::string SecondText)
        : m_FirstText(FirstText)
        , m_SecondText(SecondText)
        , m_Number(0)
    {
        std::cout << "CLogItem(std::string, std::string): " << m_FirstText << ", " << m_SecondText << "\n";
    }

    CLogItem(std::string&& FirstText, std::string&& SecondText, int Number)
        : m_FirstText(std::move(FirstText))
        , m_SecondText(std::move(SecondText))
        , m_Number(Number)
    {
        std::cout << "CLogItem(std::string&&, std::string&&): " << m_FirstText << ", " << m_SecondText << "\n";
    }

    void SetFirst(std::string FirstText)
    {
        std::cout << std::is_lvalue_reference<decltype((FirstText))>::value << std::endl;
        m_FirstText = std::move(FirstText);
    }

    void SetSecond(std::string&& SecondText)
    {
        std::cout << std::is_lvalue_reference<decltype((SecondText))>::value << std::endl;
        m_SecondText = std::move(m_SecondText);
    }

private:
    std::string m_FirstText;
    std::string m_SecondText;
    int m_Number;
};
