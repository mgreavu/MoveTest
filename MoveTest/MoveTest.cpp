#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "LogItem.h"
#include "test_string.h"
#include <iostream>
#include <string>

static void TakeByConstPointer(const test_string* ts)
{
    ts->print();
}

static void TakeByConstReference(const test_string& ts)
{
    ts.print();
}

static void TakeByValue(test_string ts)
{
    ts.print();
}

int main()
{
    CLogItem item_1("First", "Second");

    constexpr int TEST_SIZE = 16;

    char* First = new char[TEST_SIZE + 1];
    memset(First, 0, TEST_SIZE + 1);
    strncpy(First, "Alabala", strlen("Alabala"));

    char* Second = new char[TEST_SIZE + 1];
    memset(Second, 0, TEST_SIZE + 1);
    strncpy(Second, "Portocala", strlen("Portocala"));

    CLogItem item_2(First, Second);

    delete[] First;
    delete[] Second;

    std::string TheFirst("First");
    std::string TheSecond("Second");

    CLogItem item_3(TheFirst, TheSecond);
    CLogItem item_4(std::move(TheFirst), std::move(TheSecond), 7);

    std::string FirstText("AnaAreMere");
    std::cout << "First: " << FirstText << std::endl;

    item_4.SetFirst(FirstText);
    std::cout << "First: " << FirstText << std::endl;

    std::string SecondText("GigelAreCiocane");
    std::cout << "Second: " << SecondText << std::endl;

    item_4.SetSecond(std::move(SecondText));
    std::cout << "Second: " << SecondText << std::endl;

    item_4.SetFirst(std::move(FirstText));
    std::cout << "First: " << FirstText << std::endl;

    test_string t1("Alpha"), t2("Beta"), t3("Omicron");
    t1.print();
    t2.print();
    t3.print();

    test_string t4 = t3;
    t4.print();
    std::cout << "==============" << std::endl;

    test_string_container tcont1(t4);
    std::cout << "--------------" << std::endl;

    test_string_container tcont2(t1, 2765);
    std::cout << "--------------" << std::endl;

    test_string_container tcont3("alabala");
    std::cout << "--------------" << std::endl;

    test_string_container tcont5(std::move(t4));
    std::cout << "--------------" << std::endl;

    test_string_container tcont6({});
    std::cout << "--------------" << std::endl;

    const char* SomeText = "This is a test";
    test_string_container tcont7(SomeText);
    std::cout << "--------------" << std::endl;

    test_string* ts = new test_string("Pizza");
    TakeByConstPointer(ts);
    TakeByConstReference(*ts);
    TakeByValue(*ts);
    delete ts;
    std::cout << "--------------" << std::endl;

    test_string tsApple("Apple"), tsPear("Pear");
    tsApple = tsPear;
    tsApple.print();

    return 0;
}

